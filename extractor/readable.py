#!/usr/bin/env python
# -- coding: UTF-8 --

"""
Short Description:

Created on 01/07/2017

:author: hpcosta
:maintainer:  hpcosta
:version: 0.1
:status: Staging
:projectname: mobacar
"""

import logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p ::')
import re


class Readable():
	""" docstring """

	def __init__(self):
		"""Constructor for readable"""

	def to_english(self, text):
		new_text = ''
		text = text.replace('^', '')
		text = text.replace('\n', ' ')

		pat = re.compile(r'([A-Z][^\.!?]*[\.!?])', re.M)  # multiple lines and ignore case
		lines = pat.findall(text)
		for line in lines:
			line = line[0].capitalize() + line[1:].lower()
			line = re.sub(r'\s+', ' ', line)
			new_text += line + '\n'
		return new_text[:-1]

# FOR TESTING PURPOSES
if __name__ == '__main__':
	text = u'''                                                   
TWO GASOLINE REFUELLING/PREPAY OPTIONS ARE AVAILABLE ^         
ALL RENTALS START WITH A FULL GASOLINE TANK.^                  
OPTION 1. RETURN THE VEHICLE WITH A FULL TANK OR PAY A ^       
          REFUELLING CHARGE BASED ON 2.50 /GALLON.^            
OPTION 2. PURCHASE A FULL TANK AT THE TIME OF RENTAL ^         
          BASED ON GAS CHARGE OF 1.10 PER GALLON AND ^         
          RETURN THE CAR WITHOUT REFILLING THE TANK.^          
          REFUNDS ARE NOT AVAILABLE.^ 
	'''
	print(Readable().to_english(text))
