#!/usr/bin/env python
# -- coding: UTF-8 --

"""
Short Description:

Created on 01/07/2017

:author: hpcosta
:maintainer:  hpcosta
:version: 0.1
:status: Staging
:projectname: mobacar
"""

import logging
import json
import utils.files_and_folders as ff
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p ::')


class SectionIdentifier(object):
	""" docstring """

	def __init__(self, source_file):
		"""Constructor for extractor"""
		self.source_file = source_file

		# question 1
		self.rentals = self.get_rentals()

	def get_rentals(self):
		"""
		Assuming that there is no duplicates...
		:return:
		"""

		content = ff.file_reader(self.source_file)
		rentals = dict()
		rental = list()
		section_bound = '\' ----'

		for i in range(0, len(content)-1):
			line = content[i]
			if line.startswith(section_bound):
				i += 1
				contract_identifier = content[i]
				i += 1
				line = content[i]
				while not line.startswith(section_bound) and i < len(content):
					line = content[i]
					rental.append(line)
					i += 1
				rentals[contract_identifier] = rental

		print('Number of Rentals: ' + str(len(rentals)))
		print(json.dumps(rentals, sort_keys=True, indent=4))

		return rentals


# FOR TESTING PURPOSES
if __name__ == '__main__':
	source_file = '/Users/hpcosta/dev/python/assignments/mobacar/data/rentals.txt'
	e = SectionIdentifier(source_file)
