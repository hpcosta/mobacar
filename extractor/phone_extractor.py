#!/usr/bin/env python
# -- coding: UTF-8 --
"""
Short Description: Extracts Phone Number Candidates from text.

:author: hpcosta
:maintainer:  hpcosta
:version: 0.1
:projectname: mobacar
:status: Development
"""
import os
import sys
import re


class PhoneExtractor():
	"""Extracts Phone Number Candidates from text."""

	def __init__(self, nltk_postagger):
		"""Constructor for the PhoneExtractor."""
		# Needs the sentence postagged
		self.__nltk_postagger = nltk_postagger

	def extract_phone_numbers_from_text(self, text):
		"""Extracts a list of Phone numbers candidates from text.
		:param text: text to be processed
		:type text: str
		:return: Phone Number list of candidates
		:rtype: list
		"""
		# Cleaning special characters from text
		text = self.__remove_dates_from_text(text)  # Removing dates from text.
		text = self.__strip_punctuation(text)  # Removing punctuation from text.

		# Posttagging text
		tagged_sent = self.__nltk_postagger(text.split())

		# returns phone numbers bigger than 8 digits and with the tag 'CD'
		return [word for word, pos in tagged_sent if (pos == 'CD' and len(word) > 8 and word.isdigit())]

	def __remove_dates_from_text(self, text):
		"""Removes dates from text.
		:param text: text to be processed
		:type text: str
		:return: text processed without dates
		:rtype: str
		"""
		self.__is_not_used()

		# removes dates in the following format: 00-./00-/0000 or 0000-./00-./00
		text = re.sub('\D\d{4}[-\./]\d{2}[-\./]\d{2}\D', '$$$$$$$$$$$$$$$$$$$$$$', text).strip()
		text = re.sub('\D\d{2}[-\./]\d{2}[-\./]\d{4}\D', '$$$$$$$$$$$$$$$$$$$$$$', text).strip()

		# removes dates in the following format: (01-12)[ -./](1980-2019)
		text = re.sub('\D(0{0,1}[1-9]|10|11|12])[ -\./](19[8-9][0-9]|20[0-1][0-9])', '$$$$$$$$$$$$$$$$$$$$$$', text).strip()
		return text

	def __strip_punctuation(self, text):
		"""Replaces the '+' by '00' and removes other special characters from the text.
		:param text: text to be processed
		:type text: str
		:return: text without punctuation
		:rtype: str
		"""
		x = self.__clean_text(text)  # removing and replacing special characters from the text.
		splitted_text = x.split()  # splitting text into sentences.

		# Joins sets of numbers together.
		prev_word = ''
		new_text = ''
		for word in splitted_text:
			if prev_word.isdigit() and word.isdigit():
				if len(prev_word) + len(word) > 15:
					new_text = new_text + " " + word
					prev_word = word
				else:
					new_text = new_text + word
					prev_word = prev_word + word
			else:
				new_text = new_text + " " + word
				prev_word = word
		return new_text

	def __clean_text(self, text):
		"""Removes special characters from text.
		:param text: text to be processed
		:type text: str
		:return: text processed
		:rtype: str
		"""
		self.__is_not_used()

		text = text.replace('+', '00')  # replacing + by 00
		text = text.replace('.', '-')  # coz sometimes numbers are separated by final dots.
		text = text.replace('\n', ' . ')  # prevents to join numbers from different lines.

		__chars_to_remove = [u"-­‐", u"‐", u"–", u"-", u"\\", u"•", u"â­", u"▪", u"✔", u"●", u"♦", u"o", u"·", u"➢", u"▷"]
		for c in __chars_to_remove:
			# print(c)
			text = text.replace(c, " ")
			# print("TEXT: " + text)

		__re_remove_special_chars__ = re.compile("[\]\[;:\'\"\*/\),\(\|\s]+")
		text = __re_remove_special_chars__.sub(" ", text).strip()

		__re_collapse_spaces__ = re.compile("\s+")
		return __re_collapse_spaces__.sub(" ", text).strip()

	def __is_not_used(self):
		pass

#########################
# FOR TESTING PURPOSES
#########################
if __name__ == '__main__':
	try:
		from nltk import pos_tag
	except ImportError:
		print('[!] You need to install nltk (pip install nltk)')

	rawText = u""" AC-ACE           WED 08MAR /               PHONE:0034902119726 ^
AGP T001 - GOLDCAR RHODIUM                       8778223872 ^  

		"""
	phone_extractor = PhoneExtractor(pos_tag)
	print('>>> List of Phone Number Candidates:')
	print(phone_extractor.extract_phone_numbers_from_text(rawText))
