# Question #



### How to isolate each section.
* Approach
    * This is a simple task. The sections boundaries are well-formatted. 
    * We just need to verify if the line startwith('\' ----').
    * Then we create a class rental.
    * Inside rental we have subsections, which are also easy to delimit because we know that after the rental ID we have:
        * '*AIRPORT ^'
        * '*AGE ^'
        * '*CDW ^'
        * '*SPECIAL EQUIPMENT ^'
        * '*GAS ^'
        * '*HOURS ^'
        * '*INSURANCE ^'
        * '*MAKES ^'
        * '*OTHER ^'
        * '*PHONE ^'
        * '*PAYMENT ^'
        * etc.
    
    * Using string manipulation and regular expressions this can be easily done.
    * As soon as we get the subsection in the rental we can start be extraction the necessary information from each subsection or even cross information between sections.

* Accuracy: 100%

* Outcome example:
    ![example](images/sections.png)

    * see extractor/extractor

### How to format into clear readable English rather than the capitals it is entered in.
* Approach
    * Assuming that you are talking about the entire text, this can be easily manipulated.

* Accuracy: 100%

* Outcome example:
    ![example](images/readable.png)

    * see extractor/readable

### How to identify specific subjects such as cities, car types, states, Credit card identifiers, currencies etc so they are treated and formatted differently to standard text – e.g. Capitalise for city / country.
* Approach
    * This requires different techniques, for example for cities/ states/ countries:
        * we can use spacy/ nltk/ etc. (nlp libraries) to help automatically identify Named Entities;
        * then we can improve it by:
            * creating a resource using the internet (wikipedia/ webpages/ etc) or even build it manually;    
            * or buy a resource with all the names of cities and countries (governments or post offices have this info);
        
* Accuracy: unknown
    * the precision will vary from field to field   

* Outcome
    * City
    * State
    * Country
    * Car type
    * etc.

### How to extract specific fields so they can be stored in a database. Examples are age, vehicle excess, insurance charges, office location, phone numbers.
* Approach
    * Different techniques/ algorithms required.
    * Each field is different.
    * For example, phone numbers can be extracted with regular expressions (see example).
    * To extract age it is required a rule based system + regular expressions.
        - first we extract the number (age) and then look behind and ahead for words that will give us more information, such as >=21 and >=24 for luxury vehicles.
        - from the examples it seems a bit complex, but I can imagine building a training dataset and use Machine Learning here.
        - guessing that there is a finite set of ways of filling this field.    

* Accuracy: unknown
    * the precision will vary from field to field   
    * phone number: 99%
    * age: 99%
    * age restriction: 80-90%
    * office location: 99%
    * insurance charges: seems easy to extract 
    * vehicle excess: ?
    
* Outcome  
    ![example](images/phones.png)
    
    * see extractor/phone_extractor

### How to extract complex rules as those specified in the Age and Gas fields.
* Approach
    * This question is somehow similar to the previous one.
    * One thing I did not mention before is that we should start by defining what information we want to extract. For example for Gas:
        * Pick up condition: Full talk
        * Return condition: full talk, present receipt 
        * etc. 

* Accuracy: unknown

* Outcome: unknown, need to be defined
    
### The age changes and restrictions in >CRDAGP/CAC/LT01*ALLFACTS are especially complex. We’d like a view on how they could be handled.
* Approach
    * This one indeed is more complicated.
    * I need more examples to come up with an algorithm to solve this one.
        * I think the minimum age (25) and minimum driving licence age (1 and 4) are easy to extract.
        * The following sentences where the restrictions are explained (Euros per day, min and max charge) will take more time.
            * need more examples
            * define exactly what we and to extract
            * try different approaches (probably a rule-based approach will work)
            * create a dataset
            * evaluate the algorithm accuracy
            * etc.      
            
* Accuracy: unknown
    * the precision will vary from field to field  

* Outcome: unknown, need to be defined