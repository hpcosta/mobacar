#!/usr/bin/env python
# -- coding: UTF-8 --

"""
Short Description: Auxiliary functions

:author: hpcosta
"""
import codecs
import os
import logging
import csv


def file_reader(path_to_file, sentence_by_sentence=True):
	"""
	Reads the content of a file and returns the lines or the raw content in a list.
	When the content is returned the list only contains one element.
	:param path_to_file: Path to the file.
	:type path_to_file: str
	:param sentence_by_sentence: read file line by line or the entire content at once
	:type sentence_by_sentence: bool
	:return: the content of the file in a list format or False if could not open the file or it does not exist.
	:rtype: list, str or bool
	"""
	if not file_exists(path_to_file):
		logging.error("Path to file does not exist: " + path_to_file)
		return False

	if sentence_by_sentence:
		try:
			with codecs.open(path_to_file, "r", "utf-8") as f:
				lines = f.readlines()
			return [x.strip() for x in lines]
		except():
			logging.error("ERROR when trying to read the file: " + path_to_file)
			return False
	else:
		try:
			with codecs.open(path_to_file, "r", "utf-8") as content_file:
				content = content_file.read()
			return content
		# return str2unicode(content)
		except():
			logging.error("ERROR when trying to read the file: " + path_to_file)
			return False


def file_exists(path_to_file):
	"""Verifies if a a file exists.
	:param path_to_file: path to the file (with extension)
	:type path_to_file: str
	:return: True if exists, False otherwise
	:rtype: bool
	"""
	return os.path.isfile(path_to_file)


def directory_exists(path_to_directory):
	"""Verifies if a directory exists.
	:param path_to_directory: path to the directory.
	:type path_to_directory: str
	:return: True if it exists, False otherwise
	:rtype: bool
	"""
	return os.path.isdir(path_to_directory)


def get_txt_files_from_folder(path_to_directory, extension=".txt", abs_path=False):
	"""Retrieves a list of files from a given directory.
	:param path_to_directory: path to the directory.
	:type path_to_directory: str
	:param extension: by default ".txt"
	:type extension: str
	:param abs_path: if the absolute path to the files are required, change variable to True
	:type abs_path: bool
	:return: list of files
	:rtype: list()
	"""
	files_list = list()
	if not directory_exists(path_to_directory):
		logging.info("No files in the directory: " + path_to_directory)
		return files_list

	for file_name in os.listdir(path_to_directory):
		if file_name.endswith(extension):
			if abs_path:
				files_list.append(os.path.join(path_to_directory, file_name))
			else:
				files_list.append(file_name)
	return files_list


def csv_writer(data, path_to_csv_file):
	"""
	Write data to a CSV file path.
	If the file does not exist, it creates a new one.
	If it already exists, overwrites it.
	:param data: the data to be written
	:type data: list
	:param path_to_csv_file: path to the csv path
	:type path_to_csv_file: str
	:return: none
	"""
	try:
		with open(path_to_csv_file, "wb") as csv_file:
			writer = csv.writer(csv_file, delimiter=',')
			for line in data:
				writer.writerow(line)
			logging.info("SUCCESS writing the data to the file: " + path_to_csv_file)
	except (IOError, OSError) as e:
		logging.error("ERROR creating/ writing to file: " + path_to_csv_file + "\n" + str(e))


def sort_dict_by_keys(dictionary, descending=True):
	"""
	This function sorts a given dictionary by Keys and returns a representation of a dict in a list of tuples.
	:param dictionary: Dictionary to be sorted.
	:type dictionary: dict()
	:param descending: by default the dictionary will be sorted in a descending order.
	:type descending: bool
	:return: list of tuples sorted by the first element in each tuple
	:rtype: list of tuples
	"""
	return sorted(dictionary.items(), key=lambda kv: kv[0], reverse=descending)
